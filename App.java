package _20210128_SQLite;
 
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
 
public class App {
 
    public static void connect() {
 
        Connection conn = null;
        String url = "jdbc:sqlite:./src/_20210128_SQLite/mydb.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been established.");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        
    }
    public static void main(String[] args) {
        connect();
    }
}